package com.zenthrix.zenrace;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Chat {
	public static String prefix = ChatColor.GRAY + "[" + ChatColor.AQUA
			+ "Zenthrix" + ChatColor.GRAY + "] >> ";

	public static void sendMessage(Player p, String s) {
		p.sendMessage(prefix + s);
	}

	public static void noPerm(Player p) {
		p.sendMessage(prefix + "No Permissions!");
	}
}
