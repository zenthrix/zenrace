package com.zenthrix.zenrace;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Core extends JavaPlugin implements Listener {
	private static Inventory inv;

	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
	}

	
	
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		Player p = (Player) sender;
		if (label.equalsIgnoreCase("zenrace")) {
			if (p.hasPermission("zenrace.class")) {
				openGUI(p);

				Firework f = (Firework) p.getPlayer().getWorld()
						.spawn(p.getPlayer().getLocation(), Firework.class);

				FireworkMeta fm = f.getFireworkMeta();

				fm.addEffect(FireworkEffect.builder().flicker(false)
						.trail(true).with(FireworkEffect.Type.BALL_LARGE)
						.withColor(Color.GREEN).build());

				fm.setPower(1);
				f.setFireworkMeta(fm);

				p.playSound(p.getLocation(), Sound.LEVEL_UP, 1.0F, 0.0F);
			} else if (!p.hasPermission("zenrace.class")) {
				Chat.noPerm(p);
			}
		}
		return false;
	}

	public void openGUI(Player p) {
		inv = Bukkit
				.createInventory(null, 18, ChatColor.GREEN + "Pick a class");

		ItemStack item1 = new ItemStack(Material.DIAMOND_CHESTPLATE);
		ItemMeta item1Meta = item1.getItemMeta();
		item1Meta.setDisplayName(ChatColor.AQUA + "Warrior");
		item1.setItemMeta(item1Meta);

		ItemStack item1prv = new ItemStack(Material.MAP);
		ItemMeta item1Metaprv = item1prv.getItemMeta();
		item1Metaprv.setDisplayName(ChatColor.AQUA + "Preveiw Warrior");
		item1prv.setItemMeta(item1Metaprv);

		ItemStack item2 = new ItemStack(Material.POTION);
		ItemMeta item2Meta = item2.getItemMeta();
		item2Meta.setDisplayName(ChatColor.GREEN + "Knight");
		item2.setItemMeta(item2Meta);

		ItemStack item2prv = new ItemStack(Material.MAP);
		ItemMeta item2Metaprv = item2prv.getItemMeta();
		item2Metaprv.setDisplayName(ChatColor.AQUA + "Preveiw Knight");
		item2prv.setItemMeta(item2Metaprv);

		ItemStack item3 = new ItemStack(Material.BOW);
		ItemMeta item3Meta = item3.getItemMeta();
		item3Meta.setDisplayName(ChatColor.RED + "Elf");
		item3.setItemMeta(item3Meta);

		ItemStack item3prv = new ItemStack(Material.MAP); 
		ItemMeta item3Metaprv = item3prv.getItemMeta();
		item3Metaprv.setDisplayName(ChatColor.AQUA + "Preveiw Elf");
		item3prv.setItemMeta(item3Metaprv);

		ItemStack item4 = new ItemStack(Material.BLAZE_ROD);
		ItemMeta item4Meta = item4.getItemMeta();
		item4Meta.setDisplayName(ChatColor.LIGHT_PURPLE + "Mage");
		item4.setItemMeta(item4Meta);

		ItemStack item4prv = new ItemStack(Material.MAP);
		ItemMeta item4Metaprv = item4prv.getItemMeta();
		item4Metaprv.setDisplayName(ChatColor.AQUA + "Preveiw Mage");
		item4prv.setItemMeta(item4Metaprv);

		ItemStack item9 = new ItemStack(Material.PAPER);
		ItemMeta item9Meta = item9.getItemMeta();
		item9Meta.setDisplayName(ChatColor.YELLOW + "Next Page");
		item9.setItemMeta(item9Meta);

		inv.setItem(0, item1);
		inv.setItem(1, item2);
		inv.setItem(2, item3);
		inv.setItem(3, item4);
		inv.setItem(8, item9);
//		inv.setItem(9, item1prv);
//		inv.setItem(10, item2prv);
//		inv.setItem(11, item3prv);
//		inv.setItem(12, item4prv);

		p.openInventory(inv);
	}

	@EventHandler
	public void OnPlayerJoin(PlayerJoinEvent event) {
		Chat.sendMessage(event.getPlayer(),
				"Welcome! Please Select A Race /zenrace");
	
		if(!event.getPlayer().isOp()){
		this.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
			  public void run() {
					Bukkit.getServer().dispatchCommand(event.getPlayer(), "zenrace");
					
			  }
			}, 60L);
		}
		
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		
		if(inv.getName().contains(ChatColor.stripColor("kit preview"))){
			e.setCancelled(true);
			return;
		}
		
		if (!e.getInventory().getName().equalsIgnoreCase(inv.getName())) {
			return;
		}
		if (e.getCurrentItem().getItemMeta() == null) {
			return;
		}
		if (e.getCurrentItem().getItemMeta().getDisplayName()
				.equalsIgnoreCase(ChatColor.AQUA + "Warrior")) {
			e.setCancelled(true);
			p.sendMessage(Chat.prefix + ChatColor.GREEN + p.getName()
					+ " You have chosen the Warrior class!");
			p.playSound(p.getLocation(), Sound.LEVEL_UP, 1.0F, 0.0F);
			p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 300, 1));
			e.getWhoClicked().closeInventory();
			WarriorKit(p);
		}

		if (e.getCurrentItem().getItemMeta().getDisplayName()
				.equals(ChatColor.AQUA + "Preveiw Warrior")) {
			e.setCancelled(true);
			e.getWhoClicked().closeInventory();
			Bukkit.dispatchCommand((CommandSender) e.getWhoClicked(), "previewkit Warrior");
		}

		if (e.getCurrentItem().getItemMeta().getDisplayName()
				.equals(ChatColor.AQUA + "Preveiw Knight")) {
			e.setCancelled(true);
			e.getWhoClicked().closeInventory();
			Bukkit.dispatchCommand((CommandSender) e.getWhoClicked(), "previewkit Knight");
		}

		if (e.getCurrentItem().getItemMeta().getDisplayName()
				.equals(ChatColor.AQUA + "Preveiw Elf")) {
			e.setCancelled(true);
			e.getWhoClicked().closeInventory();
			Bukkit.dispatchCommand((CommandSender) e.getWhoClicked(), "previewkit Elf");
		}

		if (e.getCurrentItem().getItemMeta().getDisplayName()
				.equals(ChatColor.AQUA + "Preveiw Mage")) {
			e.setCancelled(true);
			e.getWhoClicked().closeInventory();
			Bukkit.dispatchCommand((CommandSender) e.getWhoClicked(), "previewkit Mage");
		}

		if (e.getCurrentItem().getItemMeta().getDisplayName()
				.equals(ChatColor.GREEN + "Knight")) {
			e.setCancelled(true);
			p.sendMessage(Chat.prefix + ChatColor.GREEN + p.getName()
					+ " You have chosen the Knight class!");
			p.playSound(p.getLocation(), Sound.LEVEL_UP, 1.0F, 0.0F);
			p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 300, 1));
			e.getWhoClicked().closeInventory();
			AssassinKit(p);
		}
		if (e.getCurrentItem().getItemMeta().getDisplayName()
				.equals(ChatColor.RED + "Elf")) {
			e.setCancelled(true);
			p.sendMessage(Chat.prefix + ChatColor.GREEN + p.getName()
					+ " You have chosen the Elf class!");
			p.playSound(p.getLocation(), Sound.LEVEL_UP, 1.0F, 0.0F);
			p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 300, 1));
			e.getWhoClicked().closeInventory();
			ElfKit(p);
		}
		if (e.getCurrentItem().getItemMeta().getDisplayName()
				.equals(ChatColor.LIGHT_PURPLE + "Mage")) {
			e.setCancelled(true);
			p.sendMessage(Chat.prefix + ChatColor.GREEN + p.getName()
					+ " You have chosen the Mage class!");
			p.playSound(p.getLocation(), Sound.LEVEL_UP, 1.0F, 0.0F);
			p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 300, 1));
			e.getWhoClicked().closeInventory();
			MageKit(p);
		}
		if (e.getCurrentItem().getItemMeta().getDisplayName()
				.equals("Next Page")) {
			e.setCancelled(true);
			p.playSound(p.getLocation(), Sound.SUCCESSFUL_HIT, 1.0F, 0.0F);
		}
		
		if(e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.stripColor("Back"))){
			e.setCancelled(true);
			e.getWhoClicked().closeInventory();
			Bukkit.dispatchCommand((CommandSender) e.getWhoClicked(), "zenrace");
		}
		
	}

	public void WarriorKit(Player player) {
		
		Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(),
				"manuadd " + player.getName() + " warrior");
		Bukkit.dispatchCommand(player, "pvpKit Warrior");
		
	}

	public void AssassinKit(Player player) {
		Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(),
				"manuadd " + player.getName() + " Knight");
		Bukkit.dispatchCommand(player, "pvpKit Knight");
	}

	public void ElfKit(Player player) {
		Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(),
				"manuadd " + player.getName() + " elf");
		Bukkit.dispatchCommand(player, "pvpKit elf");
	}

	public void MageKit(Player player) {
		
		
		
		Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(),
				"manuadd " + player.getName() + " mage");
		Bukkit.dispatchCommand(player, "pvpKit mage");
		
	}
}
